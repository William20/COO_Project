raja de Tamlapura - water palace
Construit par le raja de Tamlapura au milieu du XXe siècle, le site du Water Palace fut sinistré par l’éruption volcanique de 1963 et retrouve enfin son superbe, avec ses grands bassins de nénuphars, traversés par des dalles au milieu de l’eau, sa piscine royale, ses statues majestueuses et ses espaces verts luxuriants.
Silence. Respect. Serennité.
Activité historique
Prix : 12.50
