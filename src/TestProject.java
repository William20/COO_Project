import business.BusinessSpring;
import business.OfferBuilder;
import business.TripMaker;
import bean.*;
import utils.Tools;

import java.util.ArrayList;
import java.util.List;

import static business.FormMaker.createForm;

public class TestProject {

    public static void main(String[] args) {

        Form form = new Form(8,new Price(10,250), new Comfort(4,3,new TransportSetting(9, TransportType.Boat)),"historique");
        TripMaker tripMaker = (TripMaker) BusinessSpring.getBean("tripMaker");
        Tools.printOffers(tripMaker.searchOffers(form));
    }
}
