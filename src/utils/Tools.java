package utils;

import bean.*;

import java.io.*;
import java.nio.charset.Charset;
import java.util.List;

import static java.lang.Integer.parseInt;
import static java.lang.Math.abs;

/**
 * Tools class
 * Contains static methods
 */
public class Tools {

    /**
     * Calculate pertinence level of an offer
     * @param offer offer to calculate
     * @param form form compared to offer
     * @return pertinence level
     */
    public static double calculatePertinenceLevel(Offer offer, Form form) {

        int badTransport = 0;
        int overTime = 0;
        int nbOverTime = 0;

        double priceOffer = offer.getPrice();
        int nbExcursionOffer = offer.getExcursions().size();
        int hotelRangeOffer = offer.getHotel().getRange();

        int keywords = 0;

        Price priceForm = form.getPrice();
        int nbExcursionsForm = form.getComfort().getNbExcursion();
        int hotelRangeForm = form.getComfort().getHotelRange();

        for (Excursion e : offer.getExcursions()) {
            if ((e.getTransport() instanceof Bus && form.getComfort().getTransportSetting().getPreference() == TransportType.Boat)
                    || (e.getTransport() instanceof Boat && form.getComfort().getTransportSetting().getPreference() == TransportType.Bus))
                badTransport--;
            if (e.getTransport().getDuration() > form.getComfort().getTransportSetting().getDuration()) {
                overTime += e.getTransport().getDuration() - form.getComfort().getTransportSetting().getDuration();
                nbOverTime--;
            }
            keywords += compareDescriptions(e.getAbstractTouristicSite().getDescription(), form.getKeyWords());
        }
        overTime /= nbOverTime == 0 ? 1 : nbOverTime;

        double diffPrice = comparePrices(priceForm, priceOffer);
        int diffNbExcursions = compareNbExcursions(nbExcursionsForm, nbExcursionOffer);
        int diffHotelRange = compareHotelRanges(hotelRangeForm, hotelRangeOffer);

        return 0.85 * diffPrice + 0.58 * diffNbExcursions + 0.7 * diffHotelRange + 0.5 * keywords + 0.4 * overTime + 0.2 * badTransport;
    }

    /**
     * Compares prices
     * @param price min/max value to compare
     * @param concretePrice value
     * @return difference between value and min/max value
     */
    private static double comparePrices(Price price, double concretePrice) {
        double ratio = 0;
        double coef;
        double diff = 0;
        if (concretePrice > price.getPriceMax()) {
            diff = -(concretePrice - price.getPriceMax());
            ratio = diff / price.getPriceMax();
        } else if (concretePrice < price.getPriceMin()) {
            diff = (price.getPriceMin() - concretePrice);
            ratio = diff / price.getPriceMin();
        }
        if (ratio <= 0.2 && ratio > 0) {
            coef = 0.2;
        } else if (ratio <= 0.4) {
            coef = 0.5;
        } else if (ratio <= 0.6) {
            coef = 0.9;
        } else if (ratio <= 0.8) {
            coef = 1.2;
        } else {
            coef = 1.5;
        }
        return coef * diff;
    }

    /**
     * Compares number of excursions
     * @param nbExcursions number of excursions desired
     * @param concreteNbExcursions number of excursions given
     * @return abs(difference between values)
     */
    private static int compareNbExcursions(int nbExcursions, int concreteNbExcursions) {
        return -abs(nbExcursions - concreteNbExcursions);
    }

    /**
     * Compares hotel ranges
     * @param hotelRange hotel range desired
     * @param concreteHotelRange hotel range given
     * @return diff between values
     */
    private static int compareHotelRanges(int hotelRange, int concreteHotelRange) {
        if (concreteHotelRange < hotelRange) {
            return concreteHotelRange - hotelRange;
        }
        return 0;
    }

    /**
     * Compares descriptions and keywords
     * @param description description given
     * @param keywords keywords desired
     * @return Number of keywords in common
     */
    private static int compareDescriptions(String description, String keywords) {
        int result = 0;
        String[] keywordsTab = keywords.split(" ");
        for (String keyword : keywordsTab) {
            if (description.contains(keyword) && !keyword.isEmpty()) {
                result += 1;
            }
        }
        return result;
    }

    /**
     * Print offers
     * @param offers offers list
     */
    public static void printOffers(List<Offer> offers) {
        int iterator = 1;
        for (Offer offer : offers) {
            System.out.println("\nOffre n°" + iterator + " : \n");
            offer.printOffer();
            System.out.println("\n--------------------------------------------------------------------------------------------------------------------------------\n");
            iterator++;
        }
    }

    /**
     * Calulates the price of an excursions list
     * @param excursions excursions list
     * @return price
     */
    public static float calculateExcursionsPrice(List<Excursion> excursions) {
        float total = 0;
        for (Excursion excursion : excursions) {
            total += excursion.getPrice();
        }
        return total;
    }

    /**
     * Calculates hotel price during duration
     * @param hotel hotel
     * @param duration duration
     * @return price
     */
    public static float calculateDurationPrice(Hotel hotel, int duration) {
        return duration * hotel.getPrice();
    }

    /**
     * Creates a Price from a String
     * @param priceString String sent
     * @return Price
     */
    public static Price createPrice(String priceString) {
        String[] splitPrice = priceString.split("/");
        Price price = (Price) DataSpring.getBean("price");
        price.setPriceMin(parseInt(splitPrice[0]));
        price.setPriceMax(Integer.parseInt(splitPrice[1]));
        return price;
    }

    public static TransportType createPreference(String str) {
        if (str.contains("Boat")) {
            return TransportType.Boat;
        }
        if (str.contains("Bus")) {
            return TransportType.Bus;
        }
        if (str.contains("Both")) {
            return TransportType.Both;
        } else
            return null;
    }

    public static void deleteDirectory(String emplacement) {

        File path = new File(emplacement);
        if (path.exists()) {
            File[] files = path.listFiles();
            for (File file : files) {
                if (file.isDirectory()) {
                    deleteDirectory(path + "/" + file);
                } else {
                    file.delete();
                }
            }
        }
    }

    public static String readFile(String fileName, Charset charset) throws IOException {
        // Si le charset n'est pas spécifié
        if (charset == null) {
            // on utilise celui par défaut du système :
            charset = Charset.defaultCharset();
        }

        // On ouvre le fichier avec un InputStreamReader/FileInputStream
        // Ce qui permet de spécifier un encodage :
        final Reader reader = new InputStreamReader(new FileInputStream(fileName), charset);
        try {
            // On utilise un buffer pour la copie par bloc :
            char[] cbuf = new char[8192];
            // On crée le buffer qui contiendra le résultat :
            final StringBuilder result = new StringBuilder();
            // Nombre de caractère lu :
            int len;

            // boucle de lecture :
            while ((len = reader.read(cbuf)) > +0) {
                // On copie ce qu'on a lu :
                result.append(cbuf, 0, len);
            }

            return result.toString();
        } finally {
            // Dans tous les cas, on ferme le fichier :
            reader.close();
        }
    }
}
