CREATE TABLE Hotel (
  hotel_id INT NOT NULL  PRIMARY KEY,
  hotel_name VARCHAR(30),
  hotel_note INT ,
  hotel_price FLOAT,
  hotel_address VARCHAR(100));

CREATE TABLE TouristicSite(
  touristicSite_name VARCHAR(30) PRIMARY KEY,
  touristicSite_duration INT,
  touristicSite_price FLOAT);

CREATE TABLE Transport (
  transport_id INT NOT NULL PRIMARY KEY,
  transport_duration INT,
  transport_hotel_id INT,
  transport_touristicSite_id VARCHAR(30),
  transport_name VARCHAR(30),
  transport_price FLOAT,
  transport_type INT);