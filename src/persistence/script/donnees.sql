INSERT INTO Transport(transport_id, transport_duration, transport_hotel_id, transport_touristicSite_id,transport_name, transport_price, transport_type) VALUES
(1,1,1,'Le Mayeur','B50', 4.70,0),
(2,3,1,'Temple Pura Besakih','B29',4.70,0),
(3,3,1,'Île de Menjangan','C89',4.70,0),
(4,7,1,'raja de Tamlapura','D40',10.99,1),
(5,1,1,'Jimbaran','O53',2.70,0),
(6,6,1,'Pemuteran','L32',30.80,1);

INSERT INTO Transport(transport_id, transport_duration, transport_hotel_id, transport_touristicSite_id,transport_name, transport_price, transport_type) VALUES
(7,1,2,'Le Mayeur', 'B51',3.70,0),
(8,4,2,'Temple Pura Besakih','B30',8.68,0),
(9,3,2,'Île de Menjangan','C90',7.80,1),
(10,7,2,'raja de Tamlapura','D41',12.99,0),
(11,1,2,'Jimbaran','O54',2.70,0),
(12,7,2,'Pemuteran','L33',11.95,0);


INSERT INTO Transport(transport_id, transport_duration, transport_hotel_id, transport_touristicSite_id,transport_name, transport_price, transport_type) VALUES
(13,1,3,'Le Mayeur', 'B52',4.70,0),
(14,4,3,'Temple Pura Besakih','B31',9.69,0),
(15,3,3,'Île de Menjangan','C91',7.80,1),
(16,7,3,'raja de Tamlapura','D42',13.99,0),
(17,1,3,'Jimbaran','O55',2.70,0),
(18,7,3,'Pemuteran','L34',11.95,0);


INSERT INTO Transport(transport_id, transport_duration, transport_hotel_id, transport_touristicSite_id,transport_name, transport_price, transport_type) VALUES
(19,1,4,'Le Mayeur','B53', 4.70,0),
(20,4,4,'Temple Pura Besakih','B32',7.69,0),
(21,3,4,'Île de Menjangan','C92',7.60,1),
(22,7,4,'raja de Tamlapura','D43',13.99,0),
(23,1,4,'Jimbaran','O56',2.70,0),
(24,7,4,'Pemuteran','L35',10.50,0);


INSERT INTO Hotel(hotel_id, hotel_name, hotel_note, hotel_price, hotel_address) VALUES
(1,'Kuta Luxury Residence',3,55.00, 'Jalan Wana Segara, c/o Aston Kuta Hotel , 80361 Kuta, Indonésie'),
(2,'Swandewi Homestay',1,15.50,'Jl. Kartika Plaza, Samudra Lane, 80361 Kuta, Indonésie'),
(3,'Anantara Seminyak Bali Resort',5,65,'Jl. Abimanyu (Dhyana Pura), Bali, 80361 Seminyak, Indonésie'),
(4,' Desa Nyima ',2,25.00,' Jl. Bidadari II/1B, 80361 Seminyak, Indonésie');

INSERT INTO TouristicSite(touristicSite_name, touristicSite_duration, touristicSite_price) VALUES
('Le Mayeur',4,8.00), -- activité historique
('Temple Pura Besakih',6,31.27), -- activité historique
('Temple de Tamlapura',4,12.50), -- activité historique
('Île de Menjangan',5,45.00), -- activité loisir
('Jimbaran',4,3.50), -- activité loisir
('Pemuteran',6,25.70); -- activité loisir
