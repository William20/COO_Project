package persistence.data;

import java.util.Date;

public class TransportData {
    private int id ;
    private String name ;
    private int duration ;
    private int hotel_id ;
    private String touristicSite_id ;
    private float price ;
    private int type ;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public TransportData(int id, int duration, int hotel_id, String touristicSite_id, String name, float price,int type) {

        this.id = id;
        this.duration = duration ;
        this.hotel_id = hotel_id;
        this.touristicSite_id = touristicSite_id;
        this.price = price;
        this.type = type ;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getHotel_id() {
        return hotel_id;
    }

    public void setHotel_id(int hotel_id) {
        this.hotel_id = hotel_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTouristicSite_id() {
        return touristicSite_id;
    }

    public void setTouristicSite_id(String touristicSite_id) {
        this.touristicSite_id = touristicSite_id;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }



}
