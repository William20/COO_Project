package persistence.data;

import java.util.Date;

public class TouristicSiteData {

    private String id_name ;
    private int duration ;
    private float price;

    public TouristicSiteData(String id_name, int duration, float price) {
        this.id_name = id_name;
        this.duration = duration;
        this.price = price;
    }

    public String getId_name() {
        return id_name;
    }

    public void setId_name(String id_name) {
        this.id_name = id_name;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }




}
