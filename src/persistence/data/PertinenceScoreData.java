package persistence.data;

public class PertinenceScoreData implements Comparable{
    private String fileName;
    private float score;

    public PertinenceScoreData(String fileName, float score) {
        this.fileName = fileName;
        this.score = score;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }


    @Override
    public int compareTo(Object pertinenceScoreData) {
        float compareScore=((PertinenceScoreData)pertinenceScoreData).getScore();
        if (this.getScore() > compareScore){
            return -1;
        }
        else{
            return 1;
        }
    }
}
