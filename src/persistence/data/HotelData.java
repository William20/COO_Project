package persistence.data;

public class HotelData {


    public HotelData(int id, String name, int range, float price, String address) {

        this.id = id;
        this.name = name;
        this.range = range;
        this.price = price;
        this.address = address;
    }

    private int range ;
    private float price ;
    private String address;

    private  int id  ;
    private String  name ;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRange() {
        return range;
    }

    public void setRange(int range) {
        this.range = range;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


}
