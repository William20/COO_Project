package persistence.test;

import bean.*;
import business.FormMaker;
import business.HotelIterator;
import business.TouristicSiteIterator;
import business.TransportIterator;
import persistence.jdbc.PersistenceSpring;
import persistence.jdbc.ResultManager;
import persistence.jdbc.operator.OperatorJoin;

public class ResultManagerTest {
    public static void main(String[] args) {
        Form form = new Form(8,new Price(10,250), new Comfort(4,3,new TransportSetting(9, TransportType.Boat)),null);
        //Form form = FormMaker.createForm();
        ResultManager resultManager = (ResultManager) PersistenceSpring.getBean("resultmanager");
        resultManager.initDataWithForm(form);
        HotelIterator hotelIterator = resultManager.getHotelIterator();
        TransportIterator transportIterator = resultManager.getTransportIterator();
        TouristicSiteIterator touristicSiteIterator = resultManager.getTouristicSiteIterator();
        System.out.println("Les différents hotels : ");
        while (hotelIterator.hasNext()){
            System.out.println(hotelIterator.next().getName());
        }
        System.out.println("Les différents transports : ");
        while (transportIterator.hasNext()){
            System.out.println(transportIterator.next().getName());
        }
        System.out.println("Les différents sites touristiques : ");
        while (touristicSiteIterator.hasNext()){
            System.out.println(touristicSiteIterator.next().getName());
        }
    }
}
