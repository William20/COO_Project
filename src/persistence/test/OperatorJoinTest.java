package persistence.test;

import persistence.jdbc.PersistenceSpring;
import persistence.jdbc.operator.OperatorJoin;

public class OperatorJoinTest {
    public static void main(String[] args) {
        OperatorJoin operatorJoin = (OperatorJoin) PersistenceSpring.getBean("operatorjoin");
        operatorJoin.init("SELECT DISTINCT * FROM TouristicSite WITH historique");
        while (operatorJoin.next()){
            System.out.println(operatorJoin.getTouristicSiteData().getId_name());
        }
    }
}
