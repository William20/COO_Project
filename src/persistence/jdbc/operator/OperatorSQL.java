package persistence.jdbc.operator;

import persistence.data.HotelData;
import persistence.jdbc.JdbcConnection;

import java.sql.*;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class OperatorSQL {
    ResultSet result;

    public void init(String query){
        try {
            Connection conn = JdbcConnection.getConnection();
            Statement statement = conn.createStatement();
            result = statement.executeQuery(query);

        } catch (Exception e) {
            e.printStackTrace();
        }
    };

    public boolean next(){
        try {
            if (result.next()){
                return true;
            }
            else {
                return false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public ResultSet getResult() {
        return result;
    }
}
