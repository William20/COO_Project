package persistence.jdbc.operator;

import persistence.data.HotelData;
import persistence.data.PertinenceScoreData;
import persistence.data.TouristicSiteData;
import persistence.data.TransportData;
import persistence.jdbc.PersistenceSpring;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class OperatorJoin{
    private PertinenceScoreData pertinenceScoreData;
    private TouristicSiteData touristicSiteData;
    private OperatorSQL operatorSQL;
    private OperatorText operatorText;

    public void init(String query){
        operatorSQL = (OperatorSQL) PersistenceSpring.getBean("operatorsql");
        operatorText = (OperatorText) PersistenceSpring.getBean("operatortext");
        operatorSQL.init(query.split("WITH")[0]);
        operatorText.init(query.split("WITH")[1]);
    }

    public boolean next() {
        Boolean bool = false;
        if (operatorSQL.next()){
            try {
                TouristicSiteData touristicSiteDataTmp = new TouristicSiteData(operatorSQL.getResult().getString("touristicSite_name"),operatorSQL.getResult().getInt("touristicSite_duration"),operatorSQL.getResult().getFloat("touristicSite_price"));
                while (operatorText.hasNext()){
                    if (operatorText.next().getFileName().contains(touristicSiteDataTmp.getId_name())){
                        touristicSiteData = touristicSiteDataTmp;
                        pertinenceScoreData = operatorText.getPertinenceScoreData();
                        bool = true;
                    }
                }
                operatorText.reset();
                if (!bool){
                    touristicSiteData = touristicSiteDataTmp;
                    pertinenceScoreData = new PertinenceScoreData(touristicSiteData.getId_name(),0);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return true;
        }
        else {
            return false;
        }
    }

    public PertinenceScoreData getPertinenceScoreData() {
        return pertinenceScoreData;
    }

    public void setPertinenceScoreData(PertinenceScoreData pertinenceScoreData) {
        this.pertinenceScoreData = pertinenceScoreData;
    }

    public TouristicSiteData getTouristicSiteData() {
        return touristicSiteData;
    }

    public void setTouristicSiteData(TouristicSiteData touristicSiteData) {
        this.touristicSiteData = touristicSiteData;
    }
}
