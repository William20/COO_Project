package persistence.jdbc.operator;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import persistence.data.PertinenceScoreData;
import utils.Tools;

import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class OperatorText implements Iterator<PertinenceScoreData>{

    private List<PertinenceScoreData> result = new ArrayList<PertinenceScoreData>();
    private int   index;

    public  void init(String query) {
        final String INDEX_DIR = "site_description/index";
        final String FILE_DIR = "site_description";
        String fileName ;
        PertinenceScoreData pertinenceScoreData;
        float scoreFile;
        int MAX_RESULTS = 100;
        int j ;
        File folder = new File(INDEX_DIR); //Répertoire
        File file_folder = new File(FILE_DIR);
        File[] listOfFiles = file_folder.listFiles(new FichierTextFilter());
        if(!folder.isDirectory()){
            System.out.println("Impossible de créer l'index dans le répertoire");
        }

//        for(j = 0; j < listOfFiles.length ; j++){
//            System.out.println("Les fichiers en .txt sont : "+ listOfFiles[j] + "\n");
//        }

        //    Le même analyseur est utilisé pour l'indexation et la recherche
        Analyzer analyseur = new StandardAnalyzer();


        try {
            //Creation de l'index
            Path indexpath = FileSystems.getDefault().getPath(INDEX_DIR); //localisation index
            Directory index = FSDirectory.open(indexpath);  //création index sur disque


            IndexWriterConfig config = new IndexWriterConfig(analyseur);
            IndexWriter w = new IndexWriter(index, config);

            //création et indexation d'un premier document
            for(File file : listOfFiles){

                Document doc = new Document();
                doc.add(new Field("nom",file.getName(), TextField.TYPE_STORED));
                doc.add(new Field("contenu", new FileReader(file), TextField.TYPE_NOT_STORED));
                w.addDocument(doc);
                w.commit();
            }
            //Interroger l'index
            DirectoryReader ireader = DirectoryReader.open(index);
            IndexSearcher searcher = new IndexSearcher(ireader); //l'objet qui fait la recherche dans l'index

            //Parsing de la requete en un objet Query
            //"contenu" est le champ interrogé par defaut si aucun champ n'est precisé
            QueryParser qp = new QueryParser("contenu", analyseur);
            Query req = qp.parse(query);

            TopDocs resultats = searcher.search(req, MAX_RESULTS); //recherche

            // Affichage resultats
            System.out.println(resultats.totalHits + " document(s) corresponde(nt)");
            for(int i=0; i<resultats.scoreDocs.length; i++) {
                int docId = resultats.scoreDocs[i].doc;
                Document d = searcher.doc(docId);
                //System.out.println(d.get("nom") + ": score " + resultats.scoreDocs[i].score);
                fileName =d.get("nom");
                scoreFile = resultats.scoreDocs[i].score;
                pertinenceScoreData = new PertinenceScoreData(fileName,scoreFile);
                result.add(pertinenceScoreData);
            }
            // fermeture seulement quand il n'y a plus besoin d'acceder aux resultats

//            Contenu de results
//            for(int k=0; k < result.size() ; k++){
//                System.out.println("Nom : " + result.get(k).getFileName() + "score : " +result.get(k).getScore() );
//            }

            Tools.deleteDirectory(INDEX_DIR);

            ireader.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public PertinenceScoreData next() {
        if(hasNext()) {
            PertinenceScoreData pertinenceScoreData = result.get(index);
            index += 1;
            return pertinenceScoreData;
        } else {
            throw new NoSuchElementException("There are no elements size = " + result.size());
        }
    }

    public PertinenceScoreData getPertinenceScoreData(){
        if(index!=0){
            return result.get(index-1);
        }
        else{
            return result.get(index);
        }
    }

    @Override
    public boolean hasNext() {
        return !(result.size() == index);
    }

    public void reset(){
        index = 0;
    }

    private class FichierTextFilter implements FilenameFilter {

        @Override
        public boolean accept(File file, String s) {
            return s.endsWith(".txt");
        }
    }

}