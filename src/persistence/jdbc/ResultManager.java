package persistence.jdbc;

import bean.*;
import business.BusinessSpring;
import business.HotelIterator;
import business.TouristicSiteIterator;
import business.TransportIterator;
import persistence.data.HotelData;
import persistence.data.PertinenceScoreData;
import persistence.data.TouristicSiteData;
import persistence.data.TransportData;
import persistence.jdbc.operator.OperatorJoin;
import persistence.jdbc.operator.OperatorSQL;
import utils.Tools;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class ResultManager {
    private HotelIterator hotelIterator;
    private TouristicSiteIterator touristicSiteIterator;
    private TransportIterator transportIterator;
    private Boolean bool;
    private float moyPrice = 0;

    public ResultManager() {
    }

    public void initDataWithForm(Form form) {
        hotelIterator = (HotelIterator) BusinessSpring.getBean("hotelIterator");
        touristicSiteIterator = (TouristicSiteIterator) BusinessSpring.getBean("siteIterator");
        transportIterator = (TransportIterator) BusinessSpring.getBean("transportIterator");
        OperatorSQL operatorSQL = (OperatorSQL) PersistenceSpring.getBean("operatorsql");
        operatorSQL.init("SELECT AVG(touristicSite_price) as moy FROM TouristicSite");
        while (operatorSQL.next()){
            try {
                moyPrice = operatorSQL.getResult().getFloat("moy");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if(form.getKeyWords() != null && !form.getKeyWords().isEmpty()){
            if (!form.getKeyWords().isEmpty()){
                OperatorJoin operatorJoin = (OperatorJoin) PersistenceSpring.getBean("operatorjoin");
                String queryWith = makeQueryWith(form);
                operatorJoin.init(queryWith);
                ArrayList<TouristicSiteData> tmpListTouristicSite = new ArrayList<TouristicSiteData>();
                ArrayList<PertinenceScoreData> tmpListPertinenceScoreData = new ArrayList<PertinenceScoreData>();
                while(operatorJoin.next()){
                    tmpListTouristicSite.add(operatorJoin.getTouristicSiteData());
                    tmpListPertinenceScoreData.add(operatorJoin.getPertinenceScoreData());
                }
                Collections.sort(tmpListPertinenceScoreData);
                for (int index=0; index < tmpListPertinenceScoreData.size();index++){
                    for (int index2=0; index2 < tmpListTouristicSite.size();index2++){
                        if (tmpListPertinenceScoreData.get(index).getFileName().contains(tmpListTouristicSite.get(index2).getId_name())){
                            AbstractTouristicSite touristicSite = ORMBuilder.buildTouristicSite(tmpListTouristicSite.get(index2));
                            try {
                                touristicSite.setDescription(Tools.readFile("site_description/" + touristicSite.getName() + ".txt",null));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            if(touristicSiteIterator.getList().size() < 4*form.getComfort().getNbExcursion()){
                                touristicSiteIterator.add(touristicSite);
                            }
                        }
                    }
                }
                String query = makeQuery(form);
                operatorSQL = (OperatorSQL) PersistenceSpring.getBean("operatorsql");
                operatorSQL.init(query);
                while (operatorSQL.next()){
                    try {
                        HotelData hotelData = new HotelData(operatorSQL.getResult().getInt("hotel_id"), operatorSQL.getResult().getString("hotel_name"), operatorSQL.getResult().getInt("hotel_note"), operatorSQL.getResult().getFloat("hotel_price"), operatorSQL.getResult().getString("hotel_address"));
                        TouristicSiteData touristicSiteData = new TouristicSiteData(operatorSQL.getResult().getString("touristicSite_name"),operatorSQL.getResult().getInt("touristicSite_duration"),operatorSQL.getResult().getFloat("touristicSite_price"));
                        TransportData transportData = new TransportData(operatorSQL.getResult().getInt("transport_id"),operatorSQL.getResult().getInt("transport_duration"),operatorSQL.getResult().getInt("transport_hotel_id"),operatorSQL.getResult().getString("transport_touristicSite_id"), operatorSQL.getResult().getString("transport_name"),operatorSQL.getResult().getFloat("transport_price"), operatorSQL.getResult().getInt("transport_type"));
                        if (hotelIterator.size() == 0){
                            hotelIterator.add(ORMBuilder.buildHotel(hotelData));
                        }
                        while (hotelIterator.hasNext()){
                            if(hotelIterator.next().getName().equals(ORMBuilder.buildHotel(hotelData).getName())){
                                bool = true;
                            }
                        }
                        if (!bool)
                            hotelIterator.add(ORMBuilder.buildHotel(hotelData));
                        bool = false;
                        hotelIterator.reset();
                        if(transportIterator.size()==0){
                            transportIterator.add(ORMBuilder.buildTransport(transportData, hotelData, touristicSiteData));
                        }
                        while (transportIterator.hasNext()){
                            if(transportIterator.next().getName().equals(ORMBuilder.buildTransport(transportData, hotelData, touristicSiteData).getName())){
                                bool = true;
                            }
                        }
                        if (!bool)
                            transportIterator.add(ORMBuilder.buildTransport(transportData, hotelData, touristicSiteData));
                        bool = false;
                        transportIterator.reset();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }

            }
        }
        else{
            String query = makeQuery(form);
            operatorSQL = (OperatorSQL) PersistenceSpring.getBean("operatorsql");
            operatorSQL.init(query);
            while (operatorSQL.next()){
                try {
                    HotelData hotelData = new HotelData(operatorSQL.getResult().getInt("hotel_id"), operatorSQL.getResult().getString("hotel_name"), operatorSQL.getResult().getInt("hotel_note"), operatorSQL.getResult().getFloat("hotel_price"), operatorSQL.getResult().getString("hotel_address"));
                    TouristicSiteData touristicSiteData = new TouristicSiteData(operatorSQL.getResult().getString("touristicSite_name"),operatorSQL.getResult().getInt("touristicSite_duration"),operatorSQL.getResult().getFloat("touristicSite_price"));
                    TransportData transportData = new TransportData(operatorSQL.getResult().getInt("transport_id"),operatorSQL.getResult().getInt("transport_duration"),operatorSQL.getResult().getInt("transport_hotel_id"),operatorSQL.getResult().getString("transport_touristicSite_id"), operatorSQL.getResult().getString("transport_name"),operatorSQL.getResult().getFloat("transport_price"), operatorSQL.getResult().getInt("transport_type"));
                    if (hotelIterator.size() == 0){
                        hotelIterator.add(ORMBuilder.buildHotel(hotelData));
                    }
                    while (hotelIterator.hasNext()){
                        if(hotelIterator.next().getName().equals(ORMBuilder.buildHotel(hotelData).getName())){
                            bool = true;
                        }
                    }
                    if (!bool)
                        hotelIterator.add(ORMBuilder.buildHotel(hotelData));
                    bool = false;
                    hotelIterator.reset();
                    if(touristicSiteIterator.size()==0){
                        touristicSiteIterator.add(ORMBuilder.buildTouristicSite(touristicSiteData));
                    }
                    while (touristicSiteIterator.hasNext()){
                        if(touristicSiteIterator.next().getName().equals(ORMBuilder.buildTouristicSite(touristicSiteData).getName())){
                            bool = true;
                        }
                    }
                    if (!bool){
                        if(touristicSiteIterator.getList().size() < 4*form.getComfort().getNbExcursion()){
                            touristicSiteIterator.add(ORMBuilder.buildTouristicSite(touristicSiteData));
                        }
                    }
                    bool = false;
                    touristicSiteIterator.reset();
                    if(transportIterator.size()==0){
                        transportIterator.add(ORMBuilder.buildTransport(transportData, hotelData, touristicSiteData));
                    }
                    while (transportIterator.hasNext()){
                        if(transportIterator.next().getName().equals(ORMBuilder.buildTransport(transportData, hotelData, touristicSiteData).getName())){
                            bool = true;
                        }
                    }
                    if (!bool)
                        transportIterator.add(ORMBuilder.buildTransport(transportData, hotelData, touristicSiteData));
                    bool = false;
                    transportIterator.reset();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private String makeQuery(Form form){
        String query = "SELECT DISTINCT * FROM Hotel, Transport, TouristicSite WHERE hotel_price BETWEEN " + (form.getPrice().getPriceMin()-(form.getComfort().getNbExcursion()*moyPrice)) + " AND " + (form.getPrice().getPriceMax()-(form.getComfort().getNbExcursion()*moyPrice)) + " AND transport_hotel_id = hotel_id AND transport_touristicSite_id = touristicSite_name";
        return query;
    }

    private String makeQueryWith(Form form){
        String query = "SELECT DISTINCT * FROM TouristicSite WITH " + form.getKeyWords();
        return query;
    }

    public void setHotelIterator(HotelIterator hotelIterator) {
        this.hotelIterator = hotelIterator;
    }

    public void setTouristicSiteIterator(TouristicSiteIterator touristicSiteIterator) {
        this.touristicSiteIterator = touristicSiteIterator;
    }

    public void setTransportIterator(TransportIterator transportIterator) {
        this.transportIterator = transportIterator;
    }

    public HotelIterator getHotelIterator() {
        return hotelIterator;
    }

    public TouristicSiteIterator getTouristicSiteIterator() {
        return touristicSiteIterator;
    }

    public TransportIterator getTransportIterator() {
        return transportIterator;
    }
}
