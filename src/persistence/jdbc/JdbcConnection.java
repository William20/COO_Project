package persistence.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * Created by pauline on 16/01/18.
 */
public class JdbcConnection {
    private static String host = "localhost";
    private static String base = "travelagency";
    private static String user = "root";
    private static String password = "";
    private static String url = "jdbc:mysql://" + host + "/" + base;

    /**
     * Singleton instance.
     */
    private static Connection connection;

    public static Connection getConnection() {
        if (connection == null) {
            try {
                connection = DriverManager.getConnection(url, user, password);
            } catch (Exception e) {
                System.err.println("Connection failed : " + e.getMessage());
            }
        }
        return connection;
    }

}
