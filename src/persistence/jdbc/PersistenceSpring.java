package persistence.jdbc;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class PersistenceSpring {
    private static final ApplicationContext context = new ClassPathXmlApplicationContext("persistence/jdbc/persistence-spring.xml");

    public static Object getBean(String beanName) {
        return context.getBean(beanName);
    }
}
