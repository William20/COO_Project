package persistence.jdbc;

import bean.AbstractTouristicSite;
import bean.AbstractTransport;
import bean.DataSpring;
import bean.Hotel;
import persistence.data.HotelData;
import persistence.data.TouristicSiteData;
import persistence.data.TransportData;

public class ORMBuilder {
    public static Hotel buildHotel(HotelData hotelData){
        Hotel hotel = (Hotel) DataSpring.getBean("hotel");
        hotel.setAdress(hotelData.getAddress());
        hotel.setName(hotelData.getName());
        hotel.setPrice(hotelData.getPrice());
        hotel.setRange(hotelData.getRange());

        return hotel;
    }

    public static AbstractTouristicSite buildTouristicSite(TouristicSiteData touristicSiteData){
        AbstractTouristicSite touristicSite;
        touristicSite = (AbstractTouristicSite) DataSpring.getBean("leisure");
        touristicSite.setName(touristicSiteData.getId_name());
        touristicSite.setPrice(touristicSiteData.getPrice());

        return touristicSite;
    }

    public static AbstractTransport buildTransport(TransportData transportData, HotelData hotelData, TouristicSiteData touristicSiteData){
        Hotel hotel = ORMBuilder.buildHotel(hotelData);
        AbstractTouristicSite touristicSite = ORMBuilder.buildTouristicSite(touristicSiteData);
        AbstractTransport transport;
        if (transportData.getType() == 0){
            transport = (AbstractTransport) DataSpring.getBean("boat");
        }
        else if(transportData.getType() == 1){
            transport = (AbstractTransport) DataSpring.getBean("bus");
        }
        else{
            transport = (AbstractTransport) DataSpring.getBean("both");
        }
        transport.setDuration(transportData.getDuration());
        transport.setName(transportData.getName());
        transport.setPrice(transportData.getPrice());
        transport.setAbstractTouristicSite(touristicSite);
        transport.setHotel(hotel);

        return transport;
    }
}
