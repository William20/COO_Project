package business;

import bean.Form;
import bean.Offer;

import java.util.List;

/**
 * Abstract class for TripMaker
 */
public abstract class AbstractTripMaker {


    public AbstractTripMaker() {
    }

    /**
     * Abstract method that search offers from a form
     * @return list of offers corresponding to form
     */
    public abstract List<Offer> searchOffers(Form form);
}
