package business;

import bean.*;
import utils.Tools;

import java.util.*;

/**
 * OfferBuilder class
 * Build offers
 */
public class OfferBuilder {

    private List<Offer> offers;


    public OfferBuilder() {
    }

    /**
     * Builds offer list from a form
     * @param form form sent
     * @param hotelIterator hotel list
     * @param touristicSiteIterator touristic site list
     * @param transportIterator transport list
     */
    private void build(Form form, HotelIterator hotelIterator, TouristicSiteIterator touristicSiteIterator, TransportIterator transportIterator) {

        ExcursionBuilder excursionBuilder = (ExcursionBuilder) BusinessSpring.getBean("excursionBuilder");

        Hotel hotel;
        offers = new ArrayList<>();

        while (hotelIterator.hasNext()) {
            hotel = hotelIterator.next();
            excursionBuilder.init(form.getComfort().getNbExcursion(), touristicSiteIterator);

            while (!excursionBuilder.noMorePossibility()) {
                offers.add(offerMaker(hotel, excursionBuilder.retrieveResult(hotel, transportIterator), form));
            }

        }

    }

    /**
     * Sort offers by pertinenceLevel
     * @param offers offers to sort
     * @return list of sorted offers
     */
    private List<Offer> sortOffers(List<Offer> offers) {
        offers.sort(Comparator.comparing(Offer::getPertinenceLevel).reversed());
        return offers;
    }

    /**
     * Retrieve offers list
     * @param form form sent
     * @param hotelIterator hotel list
     * @param touristicSiteIterator touristic site list
     * @param transportIterator transport list
     * @return sorted offers list
     */
    public List<Offer> retrieveResult(Form form, HotelIterator hotelIterator, TouristicSiteIterator touristicSiteIterator, TransportIterator transportIterator) {
        this.build(form, hotelIterator, touristicSiteIterator, transportIterator);
        return sortOffers(this.getOffers());
    }

    /**
     * Build offer list from a form
     * @param hotel hotel desired
     * @param excursions excursions list
     * @param form form sent
     * @return offer
     */
    private Offer offerMaker(Hotel hotel, List<Excursion> excursions, Form form) {
        Offer offer = (Offer) DataSpring.getBean("offer");
        offer.setDuration(form.getDuration());
        offer.setExcursions(excursions);
        offer.setHotel(hotel);
        offer.setPrice(Tools.calculateExcursionsPrice(excursions) + Tools.calculateDurationPrice(hotel, form.getDuration()));
        offer.setPertinenceLevel((float) Tools.calculatePertinenceLevel(offer, form));
        return offer;
    }

    private List<Offer> getOffers() {
        return offers;
    }

    public void setOffers(List<Offer> offers) {
        this.offers = offers;
    }
}
