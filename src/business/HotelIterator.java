package business;

import bean.Hotel;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * HotelIterator class
 * Contains hotel list
 */
public class HotelIterator implements Iterator<Hotel>{

    private List<Hotel> list = new ArrayList<Hotel>();
    private int index;

    public HotelIterator() {
    }

    public void init() {
    }

    /**
     * Checks if end of list
     * @return 1 if list not empty
     */
    @Override
    public boolean hasNext() {
        return !(list.size() == index);
    }

    /**
     * @return next hotel in the list
     */
    @Override
    public Hotel next() {
        if(hasNext()) {
            Hotel hotel = list.get(index);
            index += 1;
            return hotel;
        } else {
            throw new NoSuchElementException("There are no elements size = " + list.size());
        }
    }

    /**
     * Resets index to 0
     */
    public void reset(){
        index = 0;
    }

    /**
     * @return list size
     */
    public int size(){
        return list.size();
    }

    /**
     * Add a hotel to the list
     * @param hotel hotel to add
     */
    public void add(Hotel hotel){
        list.add(hotel);
    }

    public List<Hotel> getList() {
        return list;
    }

    public void setList(List<Hotel> list) {
        this.list = list;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
