package business;

import bean.*;
import utils.Tools;
import java.util.Scanner;

import static java.lang.Integer.parseInt;

/**
 * FormMaker class
 * Creates a form in the console
 */
public class FormMaker {
    public static Form createForm() {
        Form form = (Form) DataSpring.getBean("form");
        Scanner sc = new Scanner(System.in);

        //BeginDate
        System.out.println("Veuillez saisir une durée de séjour (en jours) :");
        String str = sc.nextLine();
        int duration = parseInt(str);

        //Price
        System.out.println("\nVeuillez saisir une tranche de prix au format min/max (euros) :");
        str = sc.nextLine();
        Price price = Tools.createPrice(str);

        //Transport
        System.out.println("\nVeuillez saisir un temps de transport maximum (min):");
        str = sc.nextLine();
        int time = parseInt(str);

        System.out.println("\nVeuillez saisir une préférence de voyage (Both,Boat,Bus) :");
        str = sc.nextLine();
        TransportType preference = Tools.createPreference(str);

        TransportSetting set = (TransportSetting) DataSpring.getBean("transportSetting");
        set.setDuration(time);
        set.setPreference(preference);

        //hotel
        System.out.println("\nVeuillez saisir une gamme d'hotel (nombre d'étoile de 1 à 5) :");
        str = sc.nextLine();
        int range = parseInt(str);

        //excursions
        System.out.println("\nVeuillez saisir le nombre d'excursions durant le séjour :");
        str = sc.nextLine();
        int nbexcursions = parseInt(str);

        Comfort comfort = (Comfort) DataSpring.getBean("comfort");
        comfort.setTransportSetting(set);
        comfort.setHotelRange(range);
        comfort.setNbExcursion(nbexcursions);

        System.out.println("\nVeuillez saisir des mots clés :");
        str = sc.nextLine();
        String keyWords = "";
        keyWords += str;

        form.setDuration(duration);
        form.setPrice(price);
        form.setComfort(comfort);
        form.setKeyWords(keyWords);

        return form;
    }

}
