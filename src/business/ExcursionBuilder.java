package business;

import bean.*;

import java.util.ArrayList;
import java.util.List;

/**
 * ExcursionBuilder class
 * Build excursions
 */
public class ExcursionBuilder {

    private List<Excursion> excursions;
    private int nbExcursion;
    private int[] whereAmI;
    private TouristicSiteIterator siteIterator;

    public ExcursionBuilder() {
    }

    /**
     * Builds excursion list of a hotel
     * @param hotel hotel of the excursion list
     * @param transportIterator transport list
     */
    private void build(Hotel hotel, TransportIterator transportIterator) {

        AbstractTouristicSite site;
        AbstractTransport transport;
        excursions = new ArrayList<>();

        for (int i = 0; i < nbExcursion; i++) {
            site = siteIterator.get(whereAmI[i]);
            transportIterator.reset();

            while (transportIterator.hasNext()) {
                transport = transportIterator.next();

                if (transport.getAbstractTouristicSite().getName().equals(site.getName()) && transport.getHotel().getName().equals(hotel.getName())) {
                    excursions.add(excursionMaker(site, transport));
                    break;
                }
            }
        }
    }

    /**
     * Retrieves excursions list
     * @param hotel hotel of the excursions list
     * @param transportIterator transport list
     * @return excursions list
     */
    public List<Excursion> retrieveResult(Hotel hotel,TransportIterator transportIterator) {
        build(hotel, transportIterator);
        nextCombination();
        return excursions;
    }

    /**
     * Inits the builder
     * @param nbExcursion
     * @param touristicSiteIterator
     */
    public void init(int nbExcursion, TouristicSiteIterator touristicSiteIterator){
        this.nbExcursion = nbExcursion;
        siteIterator = touristicSiteIterator;
        reset();
    }

    /**
     * Creates an excursion
     * @param abstractTouristicSite Touristic site of the excursion
     * @param transport transport of travel
     * @return The excursion
     */
    private Excursion excursionMaker(AbstractTouristicSite abstractTouristicSite, AbstractTransport transport) {
        Excursion excursion = (Excursion) DataSpring.getBean("excursion");
        excursion.setAbstractTouristicSite(abstractTouristicSite);
        excursion.setTransport(transport);
        excursion.setPrice(abstractTouristicSite.getPrice() + 2 * transport.getPrice());
        return excursion;
    }

    /**
     * Indicates activities to include in excursions list
     */
    private void nextCombination() {

        int i = 0;
        if (!noMorePossibility()) {
            whereAmI[i]++;
            while (whereAmI[i] > siteIterator.size() - 1 - i) {
                whereAmI[i + 1]++;
                whereAmI[i] = whereAmI[i + 1] + 1;

                for (int j = 0; j < i; j++)
                    whereAmI[j] = whereAmI[i] + (i - j);

                i++;

            }
        }
    }

    @Deprecated
    private void nextCombination(int size) {
        int i = 0;
        if (!noMorePossibility()) {
            whereAmI[i]++;
            while (whereAmI[i] > size - 1 - i) {
                whereAmI[i + 1]++;
                whereAmI[i] = whereAmI[i + 1] + 1;

                for (int j = 0; j < i; j++)
                    whereAmI[j] = whereAmI[i] + (i - j);

                i++;

            }
        }
    }

    /**
     * Resets whereAmI tab to first combination
     */
    private void reset() {
        whereAmI = new int[nbExcursion];
        for (int i = nbExcursion - 1; i >= 0; i--)
            whereAmI[i] = nbExcursion - 1 - i;
    }

    /**
     * Return 1 if the is no more possibility
     * @return
     */
    public boolean noMorePossibility() {
        if (whereAmI[nbExcursion - 1] <= siteIterator.size() - nbExcursion - 1)
            return false;
        return true;
    }

    public List<Excursion> getExcursions() {
        return excursions;
    }

    public void setExcursions(List<Excursion> excursions) {
        this.excursions = excursions;
    }

    public int getNbExcursion() {
        return nbExcursion;
    }

    public void setNbExcursion(int nbExcursion) {
        this.nbExcursion = nbExcursion;
    }

    public int[] getWhereAmI() {
        return whereAmI;
    }

    public void setWhereAmI(int[] whereAmI) {
        this.whereAmI = whereAmI;
    }

    public TouristicSiteIterator getSiteIterator() {
        return siteIterator;
    }

    public void setSiteIterator(TouristicSiteIterator siteIterator) {
        this.siteIterator = siteIterator;
    }
}
