package business;

import bean.*;
import persistence.jdbc.PersistenceSpring;
import persistence.jdbc.ResultManager;
import java.util.List;

/**
 * TripMaker class
 * Extends AbstractTripMaker
 */
public class TripMaker extends AbstractTripMaker {

    public TripMaker() {
    }

    @Override
    public List<Offer> searchOffers(Form form) {
        ResultManager resultManager = new ResultManager();
        resultManager.initDataWithForm(form);
        OfferBuilder offerBuilder = (OfferBuilder) BusinessSpring.getBean("offerBuilder");
        return offerBuilder.retrieveResult(form, resultManager.getHotelIterator(), resultManager.getTouristicSiteIterator(), resultManager.getTransportIterator());
    }

}

