package business;

import bean.AbstractTransport;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * TranportIterator class
 * Contains TransportIterator list
 */
public class TransportIterator implements Iterator<AbstractTransport>{

    private List<AbstractTransport> list = new ArrayList<AbstractTransport>();
    private int index;


    public void init() {
    }

    /**
     * Checks if end of list
     * @return 1 if list not empty
     */
    @Override
    public boolean hasNext() {
        return !(list.size() == index);
    }

    /**
     * @return next transport in the list
     */
    @Override
    public AbstractTransport next() {
        if(hasNext()) {
            AbstractTransport transport = list.get(index);
            index += 1;
            return transport;
        } else {
            throw new NoSuchElementException("There are no elements size = " + list.size());
        }
    }

    /**
     * Resets index to 0
     */
    public void reset(){
        index = 0;
    }

    /**
     * @return list size
     */
    public int size(){
        return list.size();
    }

    /**
     * Add a touristic site to the list
     * @param transport transport to add
     */
    public void add(AbstractTransport transport){
        list.add(transport);
    }

    public List<AbstractTransport> getList() {
        return list;
    }

    public void setList(List<AbstractTransport> list) {
        this.list = list;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
