package business;

import bean.AbstractTouristicSite;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * TouristicSiteIterator class
 * Contains TouristicSite list
 */
public class TouristicSiteIterator implements Iterator<AbstractTouristicSite>{

    private List<AbstractTouristicSite> list = new ArrayList<AbstractTouristicSite>();
    private int index;

    /**
     * Checks if end of list
     * @return 1 if list not empty
     */
    @Override
    public boolean hasNext() {
        return !(list.size() == index);
    }

    /**
     * @return next touristic site in the list
     */
    @Override
    public AbstractTouristicSite next() {
        if(hasNext()) {
            AbstractTouristicSite touristicSite = list.get(index);
            index += 1;
            return touristicSite;
        } else {
            throw new NoSuchElementException("There are no elements size = " + list.size());
        }
    }

    public AbstractTouristicSite get(int index){
        return list.get(index);
    }

    /**
     * Resets index to 0
     */
    public void reset(){
        index = 0;
    }

    /**
     * @return list size
     */
    public int size(){
        return list.size();
    }

    /**
     * Add a touristic site to the list
     * @param touristicSite touristic site to add
     */
    public void add(AbstractTouristicSite touristicSite){
        list.add(touristicSite);
    }

    public List<AbstractTouristicSite> getList() {
        return list;
    }

    public void setList(List<AbstractTouristicSite> list) {
        this.list = list;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
