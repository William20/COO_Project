package test;

import bean.*;
import business.*;
import utils.Tools;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

import static java.lang.Integer.parseInt;

public class MockCore {

    private static Hotel hotel1 = createHotel("Hotel1", 4, 24, "37 rue");
    private static Hotel hotel2 = createHotel("Hotel2", 5, 41, "38 rue");
    private static Hotel hotel3 = createHotel("Hotel3", 3, 21, "39 rue");
    private static Hotel hotel4 = createHotel("Hotel4", 4, 26, "40 rue");
    private static Hotel hotel5 = createHotel("Hotel5", 2, 18, "41 rue");
    private static Hotel hotel6 = createHotel("Hotel6", 3, 22, "42 rue");

    private static Leisure leisure1 = createLeisure("DescriptionLeisure1", 20, "Leisure1");
    private static Leisure leisure2 = createLeisure("DescriptionLeisure2", 25, "Leisure2");
    private static Leisure leisure3 = createLeisure("DescriptionLeisure3", 15, "Leisure3");
    private static HistoricSite historicSite1 = createHistoricSite("DescriptionHistoricSite1", 23, "HistoricSite1");
    private static HistoricSite historicSite2 = createHistoricSite("DescriptionHistoricSite2", 18, "HistoricSite2");
    private static HistoricSite historicSite3 = createHistoricSite("DescriptionHistoricSite3", 29, "HistoricSite3");

    private static Bus bus1 = createBus("Bus1", 30, hotel1, leisure1, 8);
    private static Bus bus2 = createBus("Bus2", 33, hotel2, leisure1, 7);
    private static Bus bus3 = createBus("Bus3", 28, hotel1, leisure2, 10);
    private static Bus bus4 = createBus("Bus4", 10, hotel1, historicSite1, 5);
    private static Bus bus5 = createBus("Bus5", 5, hotel1, historicSite2, 2);
    private static Bus bus6 = createBus("Bus6", 6, hotel2, leisure1, 4);
    private static Bus bus7 = createBus("Bus7", 20, hotel2, historicSite1, 9);
    private static Bus bus8 = createBus("Bus8", 18, hotel2, historicSite2, 6);
    private static Bus bus9 = createBus("Bus9", 30, hotel1, historicSite2, 8);
    private static Boat boat1 = createBoat("Boat1", 55, hotel1, historicSite1, 19);
    private static Boat boat2 = createBoat("Boat2", 51, hotel2, historicSite2, 15);
    private static Boat boat3 = createBoat("Boat3", 47, hotel3, historicSite1, 18);

    private static Excursion ex1 = createExcursion(leisure1, bus1);
    private static Excursion ex2 = createExcursion(historicSite1, bus2);
    private static Excursion ex3 = createExcursion(leisure2, bus3);
    private static Excursion ex4 = createExcursion(historicSite2, boat1);
    private static Excursion ex5 = createExcursion(leisure3, boat2);
    private static Excursion ex6 = createExcursion(historicSite3, boat3);

    public static void sort() {
        Form form = createForm();
        List<Excursion> excursions = createExcursions();

        OfferBuilder builder = new OfferBuilder();
        Offer offer1 = createOffer(hotel1,excursions,form);
        Offer offer2 = createOffer(hotel2,excursions,form);
        Offer offer3 = createOffer(hotel3, excursions, form);

        List<Offer> result  = new ArrayList<>();
        result.add(offer1);
        result.add(offer2);
        result.add(offer3);

        Tools.printOffers(result);

        System.out.println("Niveau de pertinence pour l'offre ayant l'hotel 1 : " +Tools.calculatePertinenceLevel(offer1,form));
        System.out.println("Niveau de pertinence pour l'offre ayant l'hotel 2 : " + Tools.calculatePertinenceLevel(offer2,form));
        System.out.println("Niveau de pertinence pour l'offre ayant l'hotel 3 : " +Tools.calculatePertinenceLevel(offer3,form));

        sortOffers(result);

        Tools.printOffers(result);

    }

    public static List<Offer> generate() {
        Form form = createForm();

        HotelIterator hotelIterator = (HotelIterator) BusinessSpring.getBean("hotelIterator");
        TouristicSiteIterator touristicSiteIterator = (TouristicSiteIterator) BusinessSpring.getBean("siteIterator");
        TransportIterator transportIterator = (TransportIterator) BusinessSpring.getBean("transportIterator");

        //2 hotels
        hotelIterator.add(hotel1);
        hotelIterator.add(hotel2);

        //4 activites
        touristicSiteIterator.add(leisure1);
        touristicSiteIterator.add(leisure2);
        touristicSiteIterator.add(historicSite1);
        touristicSiteIterator.add(historicSite2);

        //8 transports
        transportIterator.add(bus1);
        transportIterator.add(bus2);
        transportIterator.add(bus3);
        transportIterator.add(bus4);
        transportIterator.add(bus5);
        transportIterator.add(bus6);
        transportIterator.add(bus7);
        transportIterator.add(bus8);
        transportIterator.add(bus9);
        transportIterator.add(boat1);
        transportIterator.add(boat2);
        transportIterator.add(boat3);


        OfferBuilder offerBuilder = (OfferBuilder) BusinessSpring.getBean("offerBuilder");
        List<Offer> result = offerBuilder.retrieveResult(form,hotelIterator,touristicSiteIterator,transportIterator);
        Tools.printOffers(result);
        return result;

    }

    private static List<Excursion> createExcursions() {
        List<Excursion> excursions = new ArrayList<>();
        
        excursions.add(ex1);
        excursions.add(ex2);
        excursions.add(ex3);
        excursions.add(ex4);
        excursions.add(ex5);
        excursions.add(ex6);

        return excursions;
    }

    private static Hotel createHotel(String name, int range, float price, String adress) {
        Hotel hotel = (Hotel) DataSpring.getBean("hotel");
        hotel.setAdress(adress);
        hotel.setName(name);
        hotel.setPrice(price);
        hotel.setRange(range);

        return hotel;
    }

    private static Bus createBus(String name, int duration, Hotel hotel, AbstractTouristicSite abstractTouristicSite, float price) {
        Bus bus = (Bus) DataSpring.getBean("bus");
        bus.setAbstractTouristicSite(abstractTouristicSite);
        bus.setDuration(duration);
        bus.setHotel(hotel);
        bus.setName(name);
        bus.setPrice(price);
        
        return bus;
    }

    private static Boat createBoat(String name, int duration, Hotel hotel, AbstractTouristicSite abstractTouristicSite, float price) {
        Boat boat = (Boat) DataSpring.getBean("boat");
        boat.setAbstractTouristicSite(abstractTouristicSite);
        boat.setDuration(duration);
        boat.setHotel(hotel);
        boat.setName(name);
        boat.setPrice(price);

        return boat;
    }

    private static Excursion createExcursion(AbstractTouristicSite abstractTouristicSite, AbstractTransport transport) {
        Excursion excursion = (Excursion) DataSpring.getBean("excursion");
        excursion.setAbstractTouristicSite(abstractTouristicSite);
        excursion.setTransport(transport);
        excursion.setPrice(abstractTouristicSite.getPrice() + 2 * transport.getPrice());

        return excursion;
    }

    private static Leisure createLeisure(String description, float price, String name) {
        Leisure leisure = (Leisure) DataSpring.getBean("leisure");
        leisure.setDescription(description);
        leisure.setName(name);
        leisure.setPrice(price);

        return leisure;
    }

    private static HistoricSite createHistoricSite(String description, float price, String name) {
        HistoricSite historicSite = (HistoricSite) DataSpring.getBean("historic");
        historicSite.setDescription(description);
        historicSite.setName(name);
        historicSite.setPrice(price);

        return historicSite;
    }

    private static Form createForm() {
        Form form = (Form) DataSpring.getBean("form");

        //BeginDate
        int duration = 10;

        //Price
        Price price = Tools.createPrice("1000/1500");

        //Transport
        int time = 20;

        TransportType preference = Tools.createPreference("Both");

        TransportSetting set = (TransportSetting) DataSpring.getBean("transportSetting");
        set.setDuration(time);
        set.setPreference(preference);

        //hotel
        int range = 4;

        //excursions
        int nbexcursions = 2;

        Comfort comfort = (Comfort) DataSpring.getBean("comfort");
        comfort.setTransportSetting(set);
        comfort.setHotelRange(range);
        comfort.setNbExcursion(nbexcursions);

        String keyWords = "musée";

        form.setDuration(duration);
        form.setPrice(price);
        form.setComfort(comfort);
        form.setKeyWords(keyWords);

        return form;
    }

    private static Offer createOffer(Hotel hotel, List<Excursion> excursions, Form form){
        Offer offer = (Offer) DataSpring.getBean("offer");
        offer.setDuration(form.getDuration());
        offer.setExcursions(excursions);
        offer.setHotel(hotel);
        offer.setPrice(Tools.calculateExcursionsPrice(excursions) + Tools.calculateDurationPrice(hotel, form.getDuration()));
        offer.setPertinenceLevel((float) Tools.calculatePertinenceLevel(offer, form));
        return offer;
    }

    private static List<Offer> sortOffers(List<Offer> offers){
        offers.sort(Comparator.comparing(Offer::getPertinenceLevel).reversed());
        return offers;
    }

}
