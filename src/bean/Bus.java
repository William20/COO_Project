package bean;

/**
 * Bus class
 */
public class Bus extends AbstractTransport {

    public Bus() {
    }

    /**
     * Constructor
     * @param name name of the bus
     * @param duration duration of travel
     * @param hotel hotel associate
     * @param abstractTouristicSite TouristicSite associate
     * @param price price of travel
     */
    public Bus(String name, int duration, Hotel hotel, AbstractTouristicSite abstractTouristicSite, float price) {
        super(name, duration, hotel, abstractTouristicSite, price);
    }
}
