package bean;

/**
 * Abstract class for touristic sites
 */
public abstract class AbstractTouristicSite {

    private String name;
    private float price;
    private String description;

    public AbstractTouristicSite() {

    }

    /**
     * Constructor
     * @param name name of the site
     * @param price price to access the site
     * @param description description of the site
     */
    public AbstractTouristicSite(String name, float price, String description) {
        this.name = name;
        this.price = price;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
