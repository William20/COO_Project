package bean;

/**
 * Hotel class
 */
public class Hotel {

    private String name;
    private int range;
    private float price;
    private String adress;

    /**
     * Constructor
     * @param name name of the hotel
     * @param range range of the hotel
     * @param price price of 1 day in the hotel
     * @param adress adress of the hotel
     */
    public Hotel(String name, int range, float price, String adress) {
        this.name = name;
        this.range = range;
        this.price = price;
        this.adress = adress;
    }

    public Hotel() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRange() {
        return range;
    }

    public void setRange(int range) {
        this.range = range;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }
}
