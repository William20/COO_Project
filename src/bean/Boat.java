package bean;


/**
 * Boat class
 * extends AbstractTransport
 */
public class Boat extends AbstractTransport {

    public Boat(){

    }

    /**
     * Constructor
     * @param name name of the boat
     * @param duration duration of travel
     * @param hotel hotel associate
     * @param abstractTouristicSite TouristicSite associate
     * @param price price of travel
     */
    public Boat(String name, int duration, Hotel hotel, AbstractTouristicSite abstractTouristicSite, float price) {
        super(name, duration, hotel, abstractTouristicSite, price);
    }
}
