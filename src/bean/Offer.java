package bean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import java.util.List;

/**
 * Offer class
 */
@ManagedBean
@RequestScoped
public class Offer {

    private Hotel hotel;
    private List<Excursion> excursions;
    private float price;
    private int duration;
    private float pertinenceLevel;

    /**
     * Constructor
     * @param hotel hotel given
     * @param excursions excursions list given
     * @param price price given
     * @param duration duration desired
     * @param pertinenceLevel pertinence calculated
     */
    public Offer(Hotel hotel, List<Excursion> excursions, float price, int duration, float pertinenceLevel) {
        this.hotel = hotel;
        this.excursions = excursions;
        this.price = price;
        this.duration = duration;
        this.pertinenceLevel = pertinenceLevel;
    }

    public Offer() {
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public List<Excursion> getExcursions() {
        return excursions;
    }

    public int getExcursionsSize() {
        return excursions.size();
    }
    public void setExcursions(List<Excursion> excursions) {
        this.excursions = excursions;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public float getPertinenceLevel() {
        return pertinenceLevel;
    }

    public void setPertinenceLevel(float pertinenceLevel) {
        this.pertinenceLevel = pertinenceLevel;
    }

    /**
     * Prints the offer (toString)
     */
    public void printOffer() {
        System.out.println("Pour un voyage de "+this.getDuration()+" jours.");
        System.out.println("Hôtel "+ this.getHotel().getRange() +" étoiles \""+this.getHotel().getName()+"\" situé au bord de mer, avec plage intégrée avec un prix de "+this.getHotel().getPrice()+" euros par nuits.");
        System.out.println("Adresse de l'hotel : "+this.getHotel().getAdress()+"\n");
        System.out.println("Cette offre de séjour comprend "+this.getExcursions().size()+" excursions :");
        for(Excursion excursion : this.getExcursions()) {
            excursion.printExcursion();
        }
        System.out.println("\nPrix total du séjour : " +this.getPrice()+" euros.\n");
    }
}
