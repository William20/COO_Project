package bean;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * DataSpring class
 * Allows to get bean from data-spring.xml
 */
public class DataSpring {
    private static final ApplicationContext context = new ClassPathXmlApplicationContext("bean/data-spring.xml");

    public static Object getBean(String beanName) {
        return context.getBean(beanName);
    }
}