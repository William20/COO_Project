package bean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;

/**
 * Form class
 */
@ManagedBean
@SessionScoped
public class Form implements Serializable {


    private int duration;
    private String keyWords;
    private Price price = new Price();
    private Comfort comfort = new Comfort();


    public Form() {
    }

    /**
     * Constructor
     *
     * @param duration field duration
     * @param price    field price
     * @param comfort  field comfort
     * @param keyWords field keywords
     */
    public Form(int duration, Price price, Comfort comfort, String keyWords) {
        this.duration = duration;
        this.price = price;
        this.comfort = comfort;
        this.keyWords = keyWords;
    }

    public String validSearch() {
//        if (duration == null || price == null || comfort == null)
//            return "invalid";
//        else
            return "valid";
    }

    public int getDuration() {
        return duration;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public Comfort getComfort() {
        return comfort;
    }

    public void setComfort(Comfort comfort) {
        this.comfort = comfort;
    }

    public String getKeyWords() {
        return keyWords;
    }

    public void setKeyWords(String keyWords) {
        this.keyWords = keyWords;
    }

    /**
     * Prints a form (toString)
     */
    public void printForm() {
        System.out.println("\nRésumé du formulaire :");
        System.out.println("\nDurée en jours :" + this.getDuration());
        System.out.println("\nPrix (euros) : de " + this.getPrice().getPriceMin() + " à " + this.getPrice().getPriceMax());
        System.out.println("\nConfort :\n" +
                "Niveau d'excursions : " + this.getComfort().getNbExcursion() +
                "\nHôtel range (étoiles): " + this.getComfort().getHotelRange() +
                "\nDurée max transport (min): " + this.getComfort().getTransportSetting().getDuration() +
                "\nPréférence transport : " + this.getComfort().getTransportSetting().getPreference() + "\n");
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public float getPriceMin() {
        return price.getPriceMin();
    }

    public float getPriceMax() {
        return price.getPriceMax();
    }

    public int getTransportDuration() {
        return comfort.getTransportSetting().getDuration();
    }

    public TransportType getPreference() {
        return comfort.getTransportSetting().getPreference();
    }

    public int getHotelRange() {
        return comfort.getHotelRange();
    }

    public int getNbExcursion() {
        return comfort.getNbExcursion();
    }

    public void setPriceMin(float priceMin) {
        price.setPriceMin(priceMin);
    }

    public void setPriceMax(float priceMax) {
        price.setPriceMax(priceMax);
    }

    public void setTransportDuration(int duration) {
        comfort.getTransportSetting().setDuration(duration);
    }

    public void setPreference(TransportType preference) {
        comfort.getTransportSetting().setPreference(preference);
    }

    public void setHotelRange(int hotelRange) {
        comfort.setHotelRange(hotelRange);
    }

    public void setNbExcursion(int nbExcursion) {
        comfort.setNbExcursion(nbExcursion);
    }

}
