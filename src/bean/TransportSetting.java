package bean;

import java.io.Serializable;
/**
 * TransportSetting class
 */

public class TransportSetting implements Serializable {

    private int duration;
    private TransportType preference;

    public TransportSetting() {
    }

    /**
     * Constructor
     * @param duration transport duration
     * @param preference transport type
     */
    public TransportSetting(int duration, TransportType preference) {
        this.duration = duration;
        this.preference = preference;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public TransportType getPreference() {
        return preference;
    }

    public void setPreference(TransportType preference) {
        this.preference = preference;
    }
}
