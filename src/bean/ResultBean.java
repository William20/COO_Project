package bean;

import business.BusinessSpring;
import business.TripMaker;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import java.util.List;

@ManagedBean
@RequestScoped
public class ResultBean {

    @ManagedProperty(value = "#{form}")
    private Form form;

    private List<Offer> offers;

    public ResultBean(){

    }

    public Form getForm() {
        return form;
    }

    public void setForm(Form form) {
        this.form = form;
        TripMaker maker = (TripMaker) BusinessSpring.getBean("tripMaker");
        offers = maker.searchOffers(form);
    }

    public List<Offer> getOffers() {
        return offers;
    }

    public void setOffers(List<Offer> offers) {
        this.offers = offers;
    }

    public int getDays() {
        return offers.get(0).getDuration();
    }
    public boolean getExist() {
        return offers.size()==0;
    }
}
