package bean;

/**
 * Leisure class
 * Extends AbstractTouristicSite
 */
public class Leisure extends AbstractTouristicSite {

    public Leisure() {
    }

    /**
     * Constructor
     * @param name name of the site
     * @param price price to access the site
     * @param description description of the site
     */
    public Leisure(String name, float price, String description) {
        super(name, price, description);
    }
}
