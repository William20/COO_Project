package bean;

/**
 * HistoricSite class
 * Extends AbstractTouristicSite
 */
public class HistoricSite extends AbstractTouristicSite {

    public HistoricSite() {
    }

    /**
     * Constructor
     * @param name name of the site
     * @param price price to access the site
     * @param description description of the site
     */
    public HistoricSite(String name, float price, String description) {
        super(name, price, description);
    }
}
