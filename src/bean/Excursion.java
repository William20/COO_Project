package bean;

/**
 * Excursion class
 */
public class Excursion {

    private AbstractTouristicSite abstractTouristicSite;
    private AbstractTransport transport;
    private float price;

    public Excursion() {
    }

    /**
     * Constructor
     * @param abstractTouristicSite touristic site of the excursion
     * @param transport transport to go to the site
     */
    public Excursion(AbstractTouristicSite abstractTouristicSite, AbstractTransport transport) {
        this.abstractTouristicSite = abstractTouristicSite;
        this.transport = transport;
        this.price = transport.getPrice()+abstractTouristicSite.getPrice();
    }

    public AbstractTouristicSite getAbstractTouristicSite() {
        return abstractTouristicSite;
    }

    public void setAbstractTouristicSite(AbstractTouristicSite abstractTouristicSite) {
        this.abstractTouristicSite = abstractTouristicSite;
    }

    public AbstractTransport getTransport() {
        return transport;
    }

    public void setTransport(AbstractTransport transport) {
        this.transport = transport;
    }

    /**
     * Prints an excursion (toString)
     */
    public void printExcursion() {
        System.out.println("\n"+this.getAbstractTouristicSite().getName()+" : ");
        System.out.println(this.getAbstractTouristicSite().getDescription());
        System.out.println("Le trajet s'effectue en "+this.getTransport().getName()+ " et dure "+this.getTransport().getDuration()+" min.");
        System.out.println("Prix total de l'excursion : "+this.getPrice()+" euros.\n");
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
