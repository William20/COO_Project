package bean;
import java.io.Serializable;

/**
 * Price class
 */
public class Price implements Serializable {

    private float priceMin;
    private float priceMax;

    public Price() {
    }

    /**
     * Constructor
     * @param priceMin minimum value
     * @param priceMax maximum value
     */
    public Price(float priceMin, float priceMax) {
        this.priceMin = priceMin;
        this.priceMax = priceMax;
    }

    public float getPriceMin() {
        return priceMin;
    }

    public void setPriceMin(float priceMin) {
        this.priceMin = priceMin;
    }

    public float getPriceMax() {
        return priceMax;
    }

    public void setPriceMax(float priceMax) {
        this.priceMax = priceMax;
    }
}
