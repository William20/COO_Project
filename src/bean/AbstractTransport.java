package bean;

/**
 * Abstract class for transport
 */
public abstract class AbstractTransport {
    private String name;
    private int duration;
    private Hotel hotel;
    private AbstractTouristicSite abstractTouristicSite;
    private float price;

    public AbstractTransport() {
    }

    /**
     * Constructor
     * @param name name of the transport
     * @param duration travel duration
     * @param hotel hotel associate
     * @param abstractTouristicSite site associate
     * @param price price of the travel
     */
    public AbstractTransport(String name, int duration, Hotel hotel, AbstractTouristicSite abstractTouristicSite, float price) {
        this.name = name;
        this.duration = duration;
        this.hotel = hotel;
        this.abstractTouristicSite = abstractTouristicSite;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public AbstractTouristicSite getAbstractTouristicSite() {
        return abstractTouristicSite;
    }

    public void setAbstractTouristicSite(AbstractTouristicSite abstractTouristicSite) {
        this.abstractTouristicSite = abstractTouristicSite;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
