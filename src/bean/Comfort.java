package bean;

import java.io.Serializable;

/**
 * Class Comfort
 */

public class Comfort implements Serializable {

    private int nbExcursion;
    private int hotelRange;
    private TransportSetting transportSetting = new TransportSetting();

    public Comfort() {
    }

    /**
     * Constructor
     * @param nbExcursion the number of excursions
     * @param hotelRange the range of the hotel
     * @param transportSetting the settings of transport
     */
    public Comfort(int nbExcursion, int hotelRange, bean.TransportSetting transportSetting) {
        this.nbExcursion = nbExcursion;
        this.hotelRange = hotelRange;
        this.transportSetting = transportSetting;
    }

    public int getNbExcursion() {
        return nbExcursion;
    }

    public void setNbExcursion(int nbExcursion) {
        this.nbExcursion = nbExcursion;
    }

    public int getHotelRange() {
        return hotelRange;
    }

    public void setHotelRange(int hotelRange) {
        this.hotelRange = hotelRange;
    }

    public TransportSetting getTransportSetting() {
        return transportSetting;
    }

    public void setTransportSetting(TransportSetting transportSetting) {
        this.transportSetting = transportSetting;
    }
}
