package bean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 * TransportType enum
 */
@ManagedBean
@SessionScoped
public enum TransportType {
    Both,
    Boat,
    Bus;
}
